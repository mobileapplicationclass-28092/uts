package umn.ac.id.uasmobilegenap;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

public class LoginActivity extends AppCompatActivity {

    Button btnlogin;
    EditText unameedit, passedit;

    @Override
    protected void onCreate(Bundle savedIstanceState) {
        super.onCreate(savedIstanceState);
        Intent intent = getIntent();
        setContentView(R.layout.login_activity);

        btnlogin = findViewById(R.id.btnlogin);
        unameedit = findViewById(R.id.unameedit);
        passedit = findViewById(R.id.passedit);

        btnlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (unameedit.getText().toString().equals("william") && passedit.getText().toString().equals("28092")) {
                    Intent newIntent = new Intent(v.getContext(), MainActivity.class);
                    LoginActivity.this.startActivity(newIntent);
                }
            }
        });
    }
}
