package umn.ac.id.uasmobilegenap;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

public class HomeActivity  extends AppCompatActivity {
    Button btnlogin, btnprofile;

    @Override
    protected void onCreate (Bundle savedIstanceState){
        super.onCreate(savedIstanceState);

        setContentView(R.layout.home_activity);

        btnlogin = findViewById(R.id.btnlogin);
        btnprofile = findViewById(R.id.btnprofile);

        btnprofile.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick( View v){
               Intent newIntent = new Intent(v.getContext(), ProfileActivity.class);
               HomeActivity.this.startActivity(newIntent);
            }
        });
        btnlogin.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick( View v){
                Intent newIntent = new Intent(v.getContext(), LoginActivity.class);
                HomeActivity.this.startActivity(newIntent);
            }
        });
    }



}
